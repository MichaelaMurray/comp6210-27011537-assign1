<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
        crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Oswald:700|Patua+One|Roboto+Condensed:700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
    <title>COMP6210-27011537-Assignment1</title>
</head>

<body>
    <h1>Place Dogs</h1>

    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link active" href="home.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="pictures.php">Pictures</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="blog.php">Dog Blog</a>
        </li>
    </ul>


    <h2>Home</h2>
    <hr>
    <h4>Our Social Sites</h4>
    <div class="row justify-content-center">
        <div>
            <a href="#" class="btn btn-default btn-lg">
                <i class="fa fa-twitter">
                    <span class="network-name">Twitter</span>
                </i>
            </a>
        </div>
        <div>
            <a href="#" class="btn btn-default btn-lg">
                <i class="fa fa-facebook">
                    <span class="network-name">Facebook</span>
                </i>
            </a>
        </div>
        <div>
            <a href="#" class="btn btn-default btn-lg">
                <i class="fa fa-tumblr">
                    <span class="network-name">Tumblr</span>
                </i>
            </a>
        </div>
    </div>

    <div class="para2">
        <h5>About Us</h5>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi velit vitae fugit, accusantium repellendus quae dolorem.
        Ratione ipsam repudiandae quidem facere in corrupti qui necessitatibus soluta placeat? Velit neque ex nesciunt. Culpa
        debitis rem suscipit similique fugit excepturi praesentium deserunt velit, nisi illum! Quae aliquam est velit repellat
        eligendi placeat amet deleniti consectetur hic natus! Aperiam consectetur vel accusantium voluptate dignissimos quos
        modi sapiente, magnam, necessitatibus maxime eveniet sint, a ab. Voluptates recusandae porro repellat commodi sint
        id, mollitia ea a amet! Error amet perferendis ab consequuntur numquam laudantium distinctio nihil omnis eum asperiores
        reprehenderit sit esse minus porro accusantium facere aspernatur ipsum corporis, sed quisquam iure aliquid beatae
        tempore excepturi. Iusto voluptatum minima cum sunt animi quidem eum, officiis est laboriosam possimus voluptatem
        alias exercitationem nisi aperiam natus veniam pariatur qui obcaecati necessitatibus autem ut dignissimos magnam.
        Magni corrupti nobis provident incidunt sequi iste laudantium odio. Hic, nam doloribus?
    </div>

    <div class="para2">
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Placeat aperiam fugit deleniti ea aliquid tempora delectus enim
        minus magni exercitationem velit obcaecati corrupti ducimus facere recusandae eos adipisci accusantium nesciunt,
        sint nihil veniam voluptatum commodi sapiente! Iste autem praesentium, voluptates illo veniam expedita id consectetur
        culpa, harum nobis at ut mollitia eveniet non? Nesciunt, numquam? Perferendis pariatur iure inventore mollitia, necessitatibus
        quasi error animi incidunt eligendi dolor? Accusantium doloremque facere et inventore, quod tempore amet aliquid
        praesentium dolor, eius reprehenderit vitae sed unde sequi. Optio sit ex pariatur quia id vel esse excepturi? Doloribus
        atque nemo maiores debitis corporis nisi nihil doloremque, adipisci ipsa animi, minus vero quae eius eos et numquam
        soluta deleniti architecto quod explicabo nulla? Quibusdam reprehenderit, iste enim ipsam voluptatibus eligendi maiores
        quidem consequuntur vel, sequi saepe praesentium. Magni, ipsum eos ad maxime praesentium unde mollitia beatae accusamus
        magnam provident ipsam cum, nam sequi optio minus!
    </div>

    <div class="para2">
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil itaque quas consectetur quidem. Autem labore et tempore fugit
        nesciunt nemo omnis! Voluptate vel debitis earum quam repellat esse blanditiis amet nostrum, doloremque illo mollitia
        sapiente numquam, distinctio vero, porro voluptatibus dolore corporis soluta veniam hic perferendis similique facilis!
        Excepturi nostrum culpa necessitatibus iure, delectus magnam molestiae quo a porro nobis, deserunt voluptatum odit!
        Cupiditate vitae et possimus asperiores, illo sit voluptas! Aliquid fugit temporibus quia tempore impedit maiores
        vel at vero quasi dignissimos accusamus atque quas nostrum incidunt porro officiis repellendus voluptatibus, magni
        ad. Porro molestias corrupti velit laborum expedita nobis maxime? Reprehenderit sed aperiam accusantium. Odio, impedit
        id quibusdam corporis ratione commodi officiis molestias explicabo! Dolor alias hic distinctio velit quisquam est
        eligendi beatae debitis voluptatibus? Labore debitis ad, sint rerum illum cum corporis, atque neque doloremque earum
        voluptatum sunt! Ea beatae aut optio accusantium a velit. Eius, eveniet.
    </div>

    <div class="para2">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque soluta magni esse doloribus quae perferendis modi dolorem?
        Quidem dolore ab consectetur placeat velit consequuntur, reprehenderit cumque alias nisi reiciendis molestiae blanditiis
        voluptas nesciunt in perspiciatis expedita assumenda quis provident nostrum est! Quaerat autem adipisci nobis mollitia,
        tempore quos impedit ut nisi sunt porro fugit molestiae harum voluptatem, fugiat recusandae accusamus quo. A nobis
        pariatur modi velit libero accusamus delectus repellat tempora, iusto reiciendis quisquam commodi magnam, fugit quae
        aut aperiam laboriosam incidunt. Veniam, maiores alias numquam consequuntur illum laudantium dolorem deleniti obcaecati
        officiis repellendus dignissimos laborum dolore autem ipsum dicta, non tenetur. Voluptatibus consequatur qui rem,
        molestiae debitis quis doloremque ratione temporibus veritatis minus. Et placeat hic ratione vel, saepe repellat
        voluptas magnam deleniti asperiores quas! Rerum officiis est quaerat deserunt voluptatum placeat fuga maiores pariatur
        illum dicta ex ipsa, distinctio soluta exercitationem voluptatibus ab reiciendis, expedita consequatur nulla eligendi.
    </div>
    <br>
    <div class="para3">Add your own dog to our ever growing collection! attach the image and give a small description to be featured on Dog Blog :)</div>

    <div class="container">
        <form action="action_page.php">

            <label for="fname">First Name</label>
            <input type="text" id="fname" name="firstname" placeholder="First name">

            <label for="lname">Last Name</label>
            <input type="text" id="lname" name="lastname" placeholder="Last name">

            <label for="email">Email Address</label>
            <input type="text" id="email" name="email" placeholder="Email address">

            <label for="file">Choose file to upload</label>
            <input type="file" id="file" name="file" multiple>

            <label for="subject">Subject</label>
            <textarea id="subject" name="subject" placeholder="Write something about your dog here. . " style="height:200px"></textarea>

            <input type="submit" value="Submit">

        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
</body>

</html>