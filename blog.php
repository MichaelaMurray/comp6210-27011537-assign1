<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
    <title>COMP6210-27011537-Assignment1</title>
</head>

<body>

    <h1>Place Dogs</h1>

    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link" href="home.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="pictures.php">Pictures</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="blog.php">Dog Blog</a>
        </li>
    </ul>

    <h2>Dog Blog</h2>

    <hr>
    <div class="imgalign">
        <img src="images/kai.JPG" alt="">
        <h3 class="name">Kai</h3>
    </div>
    <div class="para">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque laudantium nulla voluptates labore amet, delectus cupiditate
        ex quibusdam repellendus explicabo earum quos consequuntur deleniti dicta molestiae omnis quam perferendis ad possimus
        aspernatur? Labore saepe officia fuga itaque sint, nihil architecto nulla quod nisi velit voluptatem esse! Molestias,
        ab minus cumque quo officia nam laborum eligendi cum quidem earum unde expedita nostrum debitis, ratione qui quaerat
        autem eveniet! Consequatur nulla sapiente sint laborum iure, repellat sunt ullam natus pariatur ab id consequuntur
        ducimus eveniet! Vitae beatae numquam dolor voluptatibus cupiditate ipsa obcaecati, ipsum sit similique fugiat quos
        sequi quia nisi perferendis iste non? Iure provident facere magnam explicabo repellendus accusamus. Dolor corrupti
        quidem fugit itaque voluptatum nemo blanditiis eum, eius commodi quos nihil vitae, illum voluptate alias inventore.
        Cupiditate ab cumque veritatis ratione voluptatum rem pariatur est ex, harum autem sed nisi nostrum qui atque quo,
        recusandae natus nemo tenetur optio. Quos, esse. Alias cum ad iure optio quia impedit vitae iste? Animi distinctio
        facere et, voluptas temporibus, consequuntur aliquid quisquam labore dicta, quod vel. Natus corporis unde, vero quo,
        numquam sint officia laboriosam autem iste placeat aspernatur laborum. Voluptatum itaque perferendis dolorum alias
        laudantium ut rerum exercitationem animi sed possimus.
    </div>

    <div class="imgalign">
        <img src="images/karma.JPG" alt="">
        <h3 class="name">Karma</h3>
    </div>
    <div class="para">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic, consequatur sint iste, nihil ipsam magni voluptatibus repellat
        modi velit nostrum itaque et in commodi quod architecto laborum temporibus quis. Perferendis quod neque nobis repudiandae
        totam quibusdam explicabo vero, eum est blanditiis autem quia numquam natus officiis voluptatum nesciunt ea voluptate?
        Dignissimos ducimus, perspiciatis tempora nulla laboriosam porro animi assumenda obcaecati ea expedita quibusdam
        modi eveniet numquam adipisci hic fugiat, placeat dolorem accusantium excepturi, architecto repudiandae totam vero.
        Dolor qui molestias architecto nostrum vel officiis quae culpa consequatur reprehenderit maiores, laborum quisquam
        aliquid doloremque autem recusandae commodi at sunt similique debitis sapiente laboriosam deleniti cum ut! A expedita
        ab iste possimus vero, aspernatur excepturi blanditiis consequuntur nostrum animi odit necessitatibus sapiente ducimus
        et libero quis! Nisi quaerat assumenda nostrum, explicabo nobis officiis delectus atque ad dicta. Possimus quas velit
        distinctio exercitationem dolor unde soluta aspernatur beatae non consectetur qui dolores, suscipit iusto reprehenderit
        blanditiis vitae? Esse temporibus at recusandae sed itaque explicabo libero nulla quaerat ipsam, maxime corporis
        corrupti nam labore, excepturi tenetur. Enim illum reprehenderit molestiae. Laboriosam unde enim dolorem, adipisci
        aliquid temporibus, et error voluptatibus numquam placeat odio. Dolore consectetur autem tempore dolor soluta quis
        praesentium quam sapiente quisquam?
    </div>

    <div class="imgalign">
        <img src="images/molly.JPG" alt="">
        <h3 class="name">Molly</h3>
    </div>
    <div class="para">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque laudantium nulla voluptates labore amet, delectus cupiditate
        ex quibusdam repellendus explicabo earum quos consequuntur deleniti dicta molestiae omnis quam perferendis ad possimus
        aspernatur? Labore saepe officia fuga itaque sint, nihil architecto nulla quod nisi velit voluptatem esse! Molestias,
        ab minus cumque quo officia nam laborum eligendi cum quidem earum unde expedita nostrum debitis, ratione qui quaerat
        autem eveniet! Consequatur nulla sapiente sint laborum iure, repellat sunt ullam natus pariatur ab id consequuntur
        ducimus eveniet! Vitae beatae numquam dolor voluptatibus cupiditate ipsa obcaecati, ipsum sit similique fugiat quos
        sequi quia nisi perferendis iste non? Iure provident facere magnam explicabo repellendus accusamus. Dolor corrupti
        quidem fugit itaque voluptatum nemo blanditiis eum, eius commodi quos nihil vitae, illum voluptate alias inventore.
        Cupiditate ab cumque veritatis ratione voluptatum rem pariatur est ex, harum autem sed nisi nostrum qui atque quo,
        recusandae natus nemo tenetur optio. Quos, esse. Alias cum ad iure optio quia impedit vitae iste? Animi distinctio
        facere et, voluptas temporibus, consequuntur aliquid quisquam labore dicta, quod vel. Natus corporis unde, vero quo,
        numquam sint officia laboriosam autem iste placeat aspernatur laborum. Voluptatum itaque perferendis dolorum alias
        laudantium ut rerum exercitationem animi sed possimus.
    </div>

    <div class="imgalign">
        <img src="images/jack.JPG" alt="">
        <h3 class="name">Jack</h3>
    </div>
    <div class="para">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque laudantium nulla voluptates labore amet, delectus cupiditate
        ex quibusdam repellendus explicabo earum quos consequuntur deleniti dicta molestiae omnis quam perferendis ad possimus
        aspernatur? Labore saepe officia fuga itaque sint, nihil architecto nulla quod nisi velit voluptatem esse! Molestias,
        ab minus cumque quo officia nam laborum eligendi cum quidem earum unde expedita nostrum debitis, ratione qui quaerat
        autem eveniet! Consequatur nulla sapiente sint laborum iure, repellat sunt ullam natus pariatur ab id consequuntur
        ducimus eveniet! Vitae beatae numquam dolor voluptatibus cupiditate ipsa obcaecati, ipsum sit similique fugiat quos
        sequi quia nisi perferendis iste non? Iure provident facere magnam explicabo repellendus accusamus. Dolor corrupti
        quidem fugit itaque voluptatum nemo blanditiis eum, eius commodi quos nihil vitae, illum voluptate alias inventore.
        Cupiditate ab cumque veritatis ratione voluptatum rem pariatur est ex, harum autem sed nisi nostrum qui atque quo,
        recusandae natus nemo tenetur optio. Quos, esse. Alias cum ad iure optio quia impedit vitae iste? Animi distinctio
        facere et, voluptas temporibus, consequuntur aliquid quisquam labore dicta, quod vel. Natus corporis unde, vero quo,
        numquam sint officia laboriosam autem iste placeat aspernatur laborum. Voluptatum itaque perferendis dolorum alias
        laudantium ut rerum exercitationem animi sed possimus.
    </div>

    <div class="imgalign">
        <img src="images/hudson.JPG" alt="">
        <h3 class="name">Hudson</h3>
    </div>
    <div class="para">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque laudantium nulla voluptates labore amet, delectus cupiditate
        ex quibusdam repellendus explicabo earum quos consequuntur deleniti dicta molestiae omnis quam perferendis ad possimus
        aspernatur? Labore saepe officia fuga itaque sint, nihil architecto nulla quod nisi velit voluptatem esse! Molestias,
        ab minus cumque quo officia nam laborum eligendi cum quidem earum unde expedita nostrum debitis, ratione qui quaerat
        autem eveniet! Consequatur nulla sapiente sint laborum iure, repellat sunt ullam natus pariatur ab id consequuntur
        ducimus eveniet! Vitae beatae numquam dolor voluptatibus cupiditate ipsa obcaecati, ipsum sit similique fugiat quos
        sequi quia nisi perferendis iste non? Iure provident facere magnam explicabo repellendus accusamus. Dolor corrupti
        quidem fugit itaque voluptatum nemo blanditiis eum, eius commodi quos nihil vitae, illum voluptate alias inventore.
        Cupiditate ab cumque veritatis ratione voluptatum rem pariatur est ex, harum autem sed nisi nostrum qui atque quo,
        recusandae natus nemo tenetur optio. Quos, esse. Alias cum ad iure optio quia impedit vitae iste? Animi distinctio
        facere et, voluptas temporibus, consequuntur aliquid quisquam labore dicta, quod vel. Natus corporis unde, vero quo,
        numquam sint officia laboriosam autem iste placeat aspernatur laborum. Voluptatum itaque perferendis dolorum alias
        laudantium ut rerum exercitationem animi sed possimus.
    </div>

    <div class="imgalign">
        <img src="images/andy.JPG" alt="">
        <h3 class="name">Andy</h3>
    </div>
    <div class="para">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque laudantium nulla voluptates labore amet, delectus cupiditate
        ex quibusdam repellendus explicabo earum quos consequuntur deleniti dicta molestiae omnis quam perferendis ad possimus
        aspernatur? Labore saepe officia fuga itaque sint, nihil architecto nulla quod nisi velit voluptatem esse! Molestias,
        ab minus cumque quo officia nam laborum eligendi cum quidem earum unde expedita nostrum debitis, ratione qui quaerat
        autem eveniet! Consequatur nulla sapiente sint laborum iure, repellat sunt ullam natus pariatur ab id consequuntur
        ducimus eveniet! Vitae beatae numquam dolor voluptatibus cupiditate ipsa obcaecati, ipsum sit similique fugiat quos
        sequi quia nisi perferendis iste non? Iure provident facere magnam explicabo repellendus accusamus. Dolor corrupti
        quidem fugit itaque voluptatum nemo blanditiis eum, eius commodi quos nihil vitae, illum voluptate alias inventore.
        Cupiditate ab cumque veritatis ratione voluptatum rem pariatur est ex, harum autem sed nisi nostrum qui atque quo,
        recusandae natus nemo tenetur optio. Quos, esse. Alias cum ad iure optio quia impedit vitae iste? Animi distinctio
        facere et, voluptas temporibus, consequuntur aliquid quisquam labore dicta, quod vel. Natus corporis unde, vero quo,
        numquam sint officia laboriosam autem iste placeat aspernatur laborum. Voluptatum itaque perferendis dolorum alias
        laudantium ut rerum exercitationem animi sed possimus.
    </div>

    <div class="imgalign">
        <img src="images/bella.JPG" alt="">
        <h3 class="name">Bella</h3>
    </div>
    <div class="para">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque laudantium nulla voluptates labore amet, delectus cupiditate
        ex quibusdam repellendus explicabo earum quos consequuntur deleniti dicta molestiae omnis quam perferendis ad possimus
        aspernatur? Labore saepe officia fuga itaque sint, nihil architecto nulla quod nisi velit voluptatem esse! Molestias,
        ab minus cumque quo officia nam laborum eligendi cum quidem earum unde expedita nostrum debitis, ratione qui quaerat
        autem eveniet! Consequatur nulla sapiente sint laborum iure, repellat sunt ullam natus pariatur ab id consequuntur
        ducimus eveniet! Vitae beatae numquam dolor voluptatibus cupiditate ipsa obcaecati, ipsum sit similique fugiat quos
        sequi quia nisi perferendis iste non? Iure provident facere magnam explicabo repellendus accusamus. Dolor corrupti
        quidem fugit itaque voluptatum nemo blanditiis eum, eius commodi quos nihil vitae, illum voluptate alias inventore.
        Cupiditate ab cumque veritatis ratione voluptatum rem pariatur est ex, harum autem sed nisi nostrum qui atque quo,
        recusandae natus nemo tenetur optio. Quos, esse. Alias cum ad iure optio quia impedit vitae iste? Animi distinctio
        facere et, voluptas temporibus, consequuntur aliquid quisquam labore dicta, quod vel. Natus corporis unde, vero quo,
        numquam sint officia laboriosam autem iste placeat aspernatur laborum. Voluptatum itaque perferendis dolorum alias
        laudantium ut rerum exercitationem animi sed possimus.
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
</body>

</html>