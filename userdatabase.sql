USE `container-db`;

DROP TABLE IF EXISTS tbl_posts;
DROP TABLE IF EXISTS tbl_people;

CREATE TABLE tbl_people (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    FNAME VARCHAR(255) NOT NULL,
    LNAME VARCHAR(255) NOT NULL,
    EMAIL VARCHAR(255) NOT NULL,
    IMG MEDIUMBLOB NOT NULL,
    COMNT VARCHAR(100) NOT NULL,
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE tbl_posts (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    IMG MEDIUMBLOB NOT NULL,
    COMNT VARCHAR(100) NOT NULL,
    PRIMARY KEY (ID)
    FOREIGN KEY (PID) REFERENCES tbl_people(ID)

) AUTO_INCREMENT=1;

*********************************
********* CREATE PEOPLE *********
*********************************

INSERT INTO tbl_people (FNAME,LNAME, EMAIL) VALUES ('Ava', 'Langley', 'liliane52@example.org');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL) VALUES ('Cleo', 'Meadows', 'ksanford@example.net');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL) VALUES ('Jolie', 'Jacobson', 'watsica.bennie@example.org');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL) VALUES ('Ella', 'Ballard', 'bruce.swaniawski@example.net');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL) VALUES ('Cailin', 'Ellison', 'elnora41@example.org');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL) VALUES ('Quin', 'Brooks', 'elda16@example.org');

SELECT * FROM tbl_people;

*********************************
********* CREATE POSTS *********
*********************************

INSERT INTO tbl_posts (IMG, CONTENT, PID) VALUES ('img.jpeg', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 1);
INSERT INTO tbl_posts (IMG, CONTENT, PID) VALUES ('img.jpeg', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 2);
INSERT INTO tbl_posts (IMG, CONTENT, PID) VALUES ('img.jpeg', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 3);
INSERT INTO tbl_posts (IMG, CONTENT, PID) VALUES ('img.jpeg', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 4);
INSERT INTO tbl_posts (IMG, CONTENT, PID) VALUES ('img.jpeg', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 5);
INSERT INTO tbl_posts (IMG, CONTENT, PID) VALUES ('img.jpeg', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 6);

SELECT * FROM tbl_posts;