<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
    <title>COMP6210-27011537-Assignment1</title>
</head>

<body>
    <h1>Place Dogs</h1>

    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link" href="home.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="pictures.php">Pictures</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="blog.php">Dog Blog</a>
        </li>
    </ul>


    <h2>Pictures</h2>
    <hr>

    <a href="images/kai.jpg">
        <div class="img">
            <img src="images/kai.JPG" alt="">
            <h3 class="name">Kai</h3>
        </div>
    </a>

    <a href="images/karma.JPG">
        <div class="img">
            <img src="images/karma.JPG" alt="">
            <h3 class="name">Karma</h3>
        </div>
    </a>

    <a href="images/molly.JPG">
        <div class="img">
            <img src="images/molly.JPG" alt="">
            <h3 class="name">Molly</h3>
        </div>
    </a>
    <a href="images/jack.JPG">
        <div class="img">
            <img src="images/jack.JPG" alt="">
            <h3 class="name">Jack</h3>
        </div>
    </a>
    <a href="images/hudson.JPG">
        <div class="img">
            <img src="images/hudson.JPG" alt="">
            <h3 class="name">Hudson</h3>
        </div>
    </a>
    <a href="images/andy.JPG">
        <div class="img">
            <img src="images/andy.JPG" alt="">
            <h3 class="name">Andy</h3>
        </div>
    </a>
    <a href="images/bella.JPG">
        <div class="img">
            <img src="images/bella.JPG" alt="">
            <h3 class="name">Bella</h3>
        </div>
    </a>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
</body>

</html>